package com.colorata.wallman.shared

import androidx.compose.runtime.Composable

class MainActivity : GraphActivity() {
    @Composable
    override fun Content() {
        App()
    }
}