package com.colorata.wallman.wallpapers

interface WallpapersRepository {
    val wallpapers: List<WallpaperI>
}