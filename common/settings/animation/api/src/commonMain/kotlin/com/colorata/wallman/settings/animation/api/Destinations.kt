package com.colorata.wallman.settings.animation.api

import com.colorata.wallman.core.data.Destinations
import com.colorata.wallman.core.data.destination

fun Destinations.AnimationScreen() = destination("Animation")